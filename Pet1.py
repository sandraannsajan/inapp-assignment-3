import random
class Pet():
    def __init__(self,name):
        self.name=name
        self.hunger=0
        self.boredom=0
        self.sounds=[]
        self.hthres=5
        self.bthres=5

    def __str__(self):
        if self.boredom >= self.bthres and self.hunger >= self.hthres:
            return "({} is bored ({}) and hungry ({}))".format(self.name,self.boredom,self.hunger)
        elif self.boredom < self.bthres and self.hunger < self.hthres:
            return "({} is happy)".format(self.name)
        else:
            if self.hunger >= self.hthres:
                return "({} is hungry ({}))".format(self.name,self.hunger)
            else:
                return "({} is bored ({}))".format(self.name, self.boredom)


    def clock_tick(self):
        self.boredom +=1
        self.hunger +=1

    def reduce_boredom(self,red):
        if self.boredom > 0:
            self.boredom = self.boredom-red
            if self.boredom<0:
                self.boredom=0
                print("{} is entertained".format(self.name))
        else:
            print("{} is entertained".format(self.name))

    def teach(self,word,red=5):
        self.sounds.append(word)
        self.reduce_boredom(red)

    def hi(self,red=5):
        print("{} says {}".format(self.name,random.choice(self.sounds)))
        self.reduce_boredom(red)

    def reduce_hunger(self,amt):
        if self.boredom > 0:
            self.hunger=self.hunger-amt
            print("Fed")
            if self.hunger<0:
                self.hunger=0
                print("{} is full".format(self.name))

        else:
            print("{} is full".format(self.name))


    def relieve_hunger(self):
        amt=int(input("How much to feed:"))
        self.reduce_hunger(amt)

a='y'
e='y'
count=0
s=['','','','','']
while e=='y':
    while a=='y':
        print("\nMenu\n1.Adopt a pet\n2.Interact with your adopted pet")
        op1=int(input("Enter option number: "))
        if op1==1:
            n=input("\nEnter name for pet: ")
            s[count]=Pet(n)
            s[count].clock_tick()
            print(s[count])
            count=count+1
        else:
            print("\nList of pets")
            for i in range(0, count):
                print("{}.{}".format(i + 1, s[i].name))
                s[i].clock_tick()
                print(s[i])
            pet = int(input("Which pet: "))
            print("\nSub Menu")
            print("1.Greet\n2.Teach\n3.Feed")
            op2=int(input("Enter option number: "))
            if op2==1:
                s[pet-1].hi()
                for i in range(0,count):
                    s[i].clock_tick()
                    print(s[i])
            if op2==2:
                w=input("\nEnter word: ")
                s[pet-1].teach(w)
                for i in range(0,count):
                    s[i].clock_tick()
                    print(s[i])
            if op2==3:
                s[pet-1].relieve_hunger()
                for i in range(0,count):
                    s[i].clock_tick()
                    print(s[i])
        a=input("\nDo you want to continue(y/n): ")
        if a=='n':
            e='n'
            break